package polydemo;

class Addition {
    int add(int a, int b) {
        return a + b;
    }

    float add(float a, float b) {
        return a + b;
    }

    int add(int a, int b, int c) {
        return a + b + c;
    }

    String add(String a, String b) {
        return a + b;
    }
}

public class Main {
    // method overloading -> compile time polymorphism
    public static void main(String[] args) {
        Addition obj = new Addition();
        System.out.println(obj.add(90, 10));
        System.out.println(obj.add("hello ", "world"));
    }
}
