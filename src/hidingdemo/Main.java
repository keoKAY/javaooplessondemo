package hidingdemo;

class Teacher{
    private int teacherID;
    private String name;
    private String gender;
    private int age;

    Teacher(){}
    Teacher(int teacherID , String name, String gender, int age){
        this.teacherID = teacherID;
        this.name = name;
        this.gender = gender;
        this.age = age;
    }


    // getter -> only read
    // setter -> only write


    public int getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(int teacherID) {
        this.teacherID = teacherID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age<=0) {
            System.out.println("Age must be positive and not zero ");
        }else{
            this.age = age;
        }

    }
}

public class Main {
    public static void main(String[] args) {
//        Teacher teacher = new Teacher(1001,"James ","male",45);
        Teacher teacher = new Teacher();
        teacher.setAge(-18);
        System.out.println("Age is : "+teacher.getAge());

    }
}
