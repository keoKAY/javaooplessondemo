package inheritancedemo;

import java.util.ArrayList;
import java.util.Scanner;

class Person {
    String name, gender;
    int age;

    Person() {
    }

    Person(String name, String gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    Person addRecord(Scanner input) {
        System.out.println("Enter Name : ");
        name = input.nextLine();
        System.out.println("Enter Gender: ");
        gender = input.nextLine();
        System.out.println("Enter age:");
        age = input.nextInt();
        return this;
    }

    void showRecord() {
        System.out.println("Name is : " + name);
        System.out.println("Gender is : " + gender);
        System.out.println("Age is : " + age);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                '}';
    }
}

class Worker extends Person {
    int workerId;
    float hours;
    float wage;

    Worker() {
    }

    Worker(int workerId, String name, String gender, int age, float hours, float wage) {
        super(name, gender, age); // call parent constructor
        this.workerId = workerId;
        this.hours = hours;
        this.wage = wage;
    }

    // this vs super
    Worker addRecord(Scanner input) {
        System.out.println("Enter Worker ID :");
        workerId = input.nextInt();
        super.addRecord(input); // called from the Person class.
        System.out.println("Enter Worker Hours: ");
        hours = input.nextFloat();
        System.out.println("Enter Worker Wage: ");
        wage = input.nextFloat();
        return this; // this represents the current obj.
    }

    void showRecord() {
        System.out.println("Worker ID : " + workerId);
        super.showRecord(); // call its parent method !
        System.out.println("Worker Hours: " + hours);
        System.out.println("Worker Wage : " + wage);

    }
}

class Student extends Person {
    int studentID;
    String className;
    float scores;

    Student() {
    }

    Student(int studentID, String name, String gender, int age, String className, float score) {
        super(name, gender, age);
        this.studentID = studentID;
        this.className = className;
        this.scores = score;
    }

    Student addRecord(Scanner input) {
        System.out.println("Enter Student ID: ");
        studentID = input.nextInt();
        super.addRecord(input);
        input.nextLine();
        System.out.println("Enter Student Classname : ");
        className = input.nextLine();
        System.out.println("Enter Student Score :");
        scores = input.nextFloat();
        return this;
    }

    void showRecord() {
        System.out.println("Student ID is : " + studentID);
        super.showRecord();
        System.out.println("Student ClassName : " + className);
        System.out.println("Student Score :" + scores);
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Person person = new Worker();
        //   person = new Student();
        //  person = new Person();

        if (person instanceof Worker) {
            System.out.println("This is Worker Object !!! ");
        }


    }
}
